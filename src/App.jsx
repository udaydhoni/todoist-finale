import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import SharedLayout from './Components/SharedLayout/SharedLayout'
import { Stack } from '@mui/system'
import SideBar from './Components/SharedLayout/SideBar'
import { Box } from '@mui/material'
import InboxFinal from './Components/Inbox'
import { BrowserRouter,Route } from 'react-router-dom'
import TodayFinal from './Components/Today'
import UpcomingFinal from './Components/Upcoming'
import WrapProjects from './Components/Projects/Projects'

import SingleProjectFinal from './Components/Projects/SingleProject'


function App() {
  return (
    <BrowserRouter>
      
        <Route path='/' exact><TodayFinal></TodayFinal></Route>
        <Route path='/inbox' ><InboxFinal></InboxFinal></Route>
        <Route path='/upcoming' ><UpcomingFinal></UpcomingFinal></Route>
        <Route path='/projects' exact><WrapProjects></WrapProjects></Route>
        <Route path='/projects/:singleProject' component={SingleProjectFinal}></Route>
      
    </BrowserRouter>
    
  )
}

export default App
