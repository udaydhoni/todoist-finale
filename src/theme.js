import { createTheme } from "@mui/material";
import { grey, red,} from "@mui/material/colors";

export const theme = createTheme({
    palette:{
        primary:{
            main: red[400],
            light: red[200]
        },
        secondary: {
            main: grey[900],
            light: grey[200]
        },
        teritiary: {
            main: grey[200]
        },
        other: {
            main: '#fff'
        }

    }
})