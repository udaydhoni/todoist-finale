import React from "react";
import { AppBar, CssBaseline, Toolbar, Typography,InputBase } from "@mui/material";
import MenuIcon from '@mui/icons-material/Menu';
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import {styled,Grid} from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import AddIcon from '@mui/icons-material/Add';
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';
import NotificationsNoneOutlinedIcon from '@mui/icons-material/NotificationsNoneOutlined';
import Button from '@mui/material/Button';
import SideBar from "./SideBar";

import './SharedLayout.css'
import { Link } from "react-router-dom";

const Search = styled('div')(({theme})=>({
    backgroundColor: 'white',
    padding: "5px 5px",
    borderRadius: theme.shape.borderRadius,
    width:'75%'
}))

class SharedLayout extends React.Component {
    constructor(props) {
        super()
    }
    render () {
        return (
            <div position='relative' className="w-sm-100">
            <AppBar position="sticky">
                <CssBaseline></CssBaseline>
                <Toolbar className="tool-bar">
                    <div className="left" >
                        <Button color="other" className="d-none d-md-block"><MenuIcon/></Button>
                        <Button color="other" className="d-md-none">
                        
                            <div className="btn-group">
                                <button type="button" class="btn " data-bs-toggle="dropdown" aria-expanded="false">
                                    <MenuIcon color={'other'}/>
                                </button>
                                <ul class="dropdown-menu" style={{width:'100vw',height:'100vh',backgroundColor:'rgb(246, 246, 246)'}}>
                                    <li><Link to='/inbox'class="dropdown-item" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="blue" class="bi bi-inbox" viewBox="0 0 16 16">
                                <path d="M4.98 4a.5.5 0 0 0-.39.188L1.54 8H6a.5.5 0 0 1 .5.5 1.5 1.5 0 1 0 3 0A.5.5 0 0 1 10 8h4.46l-3.05-3.812A.5.5 0 0 0 11.02 4H4.98zm9.954 5H10.45a2.5 2.5 0 0 1-4.9 0H1.066l.32 2.562a.5.5 0 0 0 .497.438h12.234a.5.5 0 0 0 .496-.438L14.933 9zM3.809 3.563A1.5 1.5 0 0 1 4.981 3h6.038a1.5 1.5 0 0 1 1.172.563l3.7 4.625a.5.5 0 0 1 .105.374l-.39 3.124A1.5 1.5 0 0 1 14.117 13H1.883a1.5 1.5 0 0 1-1.489-1.314l-.39-3.124a.5.5 0 0 1 .106-.374l3.7-4.625z"/>
                                </svg>&emsp;Inbox</Link></li>
                                    <li><Link to='/' class="dropdown-item" href="#">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="green" class="bi bi-calendar-event" viewBox="0 0 16 16">
                                <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z"/>
                                <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
                                </svg>&emsp;Today</Link></li>
                                    <li><Link to='/upcoming' class="dropdown-item" href="#">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="purple" class="bi bi-calendar" viewBox="0 0 16 16">
                                <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
                                </svg>&emsp;Upcoming</Link></li>
                                </ul>
                            </div>
                        </Button>
                        <Link to='/'><Button color='other' ><HomeOutlinedIcon/></Button></Link>
                        <Search placeholder='Search' className="d-none d-md-block">
                    <Grid container alignItems='center'>
                        <Grid item>
                            <SearchIcon color='primary' fontSize="small" sx={{marginTop:'5px'}}></SearchIcon>   
                        </Grid>
                        <Grid item>
                            <InputBase placeholder='Search'></InputBase>
                        </Grid>
                    </Grid>
                    </Search>
                    <InputBase placeholder="Search" className="d-md-none bg-white rounded p-1"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
  <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
</svg></InputBase>
                    </div>
                    <div>
                        <div className="right">
                        <Button color="other" className="d-none d-md-block"><AddIcon></AddIcon></Button>
                        <Button color="other" className="d-none d-md-block"><HelpOutlineIcon /></Button>
                        <Button color="other" className="d-none d-md-block"><NotificationsNoneOutlinedIcon/></Button>
                        </div>
                        
                    </div>
                </Toolbar>
            </AppBar>
            </div>
            
            
        )
    }
}
export default SharedLayout