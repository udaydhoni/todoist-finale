import React from "react";
import {Box, List, ListItem, ListItemButton, ListItemIcon, Typography} from '@mui/material'
import InboxOutlinedIcon from '@mui/icons-material/InboxOutlined';
import TodayIcon from '@mui/icons-material/Today';
import CalendarMonthOutlinedIcon from '@mui/icons-material/CalendarMonthOutlined';
import AddIcon from '@mui/icons-material/Add';
import ArrowDropDownOutlinedIcon from '@mui/icons-material/ArrowDropDownOutlined';
import './SideBar.css'
import { NavLink } from "react-router-dom";

class SideBar extends React.Component {
    constructor(props){
        super()
    }
    render () {
        return(
            <Box sx={{ backgroundColor:'#f5f5f5', height:'100vh'}}  flex={3} className="d-none d-md-block">
                <Box  >
                    <List>
                        <NavLink to='/inbox' style={({isActive})=>{return {color: isActive ? 'red':'black'}}} className='navLink'>
                            <ListItem disablePadding>
                                <ListItemButton ><InboxOutlinedIcon sx={{marginRight:'1rem'}}/>Inbox</ListItemButton>
                            </ListItem>
                        </NavLink>
                        <NavLink to='/' style={({isActive})=>{return {color: isActive ? 'red':'black'}}} className='navLink'>
                            <ListItem disablePadding>
                                <ListItemButton><TodayIcon sx={{marginRight:'1rem'}}/>Today</ListItemButton>
                            </ListItem>
                        </NavLink>
                        
                       <NavLink to='/upcoming' style={({isActive})=>{return {color: isActive ? 'red':'black'}}} className='navLink'>
                            <ListItem disablePadding>
                                <ListItemButton><CalendarMonthOutlinedIcon sx={{marginRight:'1rem'}}/>Upcoming</ListItemButton>
                            </ListItem>
                            
                        </NavLink> 
                        <NavLink to='/projects' style={({isActive})=>{return {color: isActive ? 'red':'black'}}} className='navLink'><ListItem disablePadding>
                                <ListItemButton>Projects</ListItemButton>
                        </ListItem>
                        </NavLink>
                    
                </List>
                </Box>
                
            </Box>
        )
    }
}

export default SideBar