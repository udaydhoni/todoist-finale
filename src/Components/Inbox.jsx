import { Box, Button, InputBase, List, ListItem, ListItemButton, MenuItem, Select, Stack, TextField, Typography,Checkbox, Container, InputLabel } from "@mui/material";
import React from "react";
import AddIcon from '@mui/icons-material/Add'
import TextareaAutosize from '@mui/base/TextareaAutosize';
import './Inbox.css'
import {  ConnectedTvOutlined, ThirtyFpsSelect } from "@mui/icons-material";
import CircleOutlinedIcon from '@mui/icons-material/CircleOutlined';
import DeleteIcon from '@mui/icons-material/Delete';
import ModeEditOutlinedIcon from '@mui/icons-material/ModeEditOutlined';
import { completeTaskHandler, deleteTaskHandler, postTaskHandler, updateTaskHandler} from "../Redux/Reducers";

// import { recieving_data } from "../Redux/Reducers";
import LinearProgress from '@mui/material/LinearProgress';
import {getProjectHandler,getTaskHandler } from "../Redux/Reducers";
import store from "../Redux/Reducers";
import { Provider, connect } from 'react-redux'
import SharedLayout from "./SharedLayout/SharedLayout";
import SideBar from "./SharedLayout/SideBar";
import Divider from '@mui/material/Divider'




class Inbox extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            dateVisible: false,
            dButtonVisible: true,
            checked: false,
            addTask: false,
            taskText: '',
            descriptionText:'',
            editActive: false,
            date: '',
            destination: '2241494367'
        }
    }
   
    componentDidMount () {
        this.props.projectObtainer()
        this.props.tasksObtainer()
       
    }
    addTaskHandler = ()=>{
        this.setState({
            addTask:true
        })
        
    }
    dateDisplayer = ()=>{
        this.setState({
            dateVisible: !this.state.dateVisible,
            dButtonVisible: this.state.dateVisible
        })
    }
    cancelHandler = ()=>{
        this.setState({
            dateVisible: !this.state.dateVisible,
            dButtonVisible: this.state.dateVisible
        })
    }
    closeHandler = ()=>{
        this.setState({
            addTask:false
        })
    }
    inputHandler = (event)=>{
        this.setState({
            taskText:event.target.value
        })
    }
    descriptionHandler = (event)=>{
        this.setState({
            descriptionText:event.target.value
        })
    }
    dateHandler = (event)=>{
        this.setState({
            date: event.target.value
        })
    }
    deleteHandler = (event)=>{
        console.log(event.target.id)
        
    }
    editHandler = (obj)=>{
        console.log(obj)
        this.setState({
         editActive: obj.editId,
        taskText: obj.editName,
        descriptionText:obj.editDes,
        date:obj.editDate

        })
    }
    closeUpdateHandler = ()=>{
        this.setState({
            editActive: '',
        })
    }
    updateHandler = (event)=>{
        this.props.updateTask(event.target.id,{
            content: this.state.taskText,
            description: this.state.descriptionText,
            due_date: this.state.date
        })
        this.setState({
            taskText:'',
            descriptionText:'',
            date: '',
            editActive:''
        })
    }
    completeHandler = (event)=>{
        this.props.completeTask(event.target.id)
    }
    destinationHandler = (event)=>{
        this.setState({
            destination: event.target.value
        })
    }
    submitHandler = ()=>{
        if (this.state.taskText) {
            this.props.addTask({
                content:this.state.taskText,
                description: this.state.descriptionText,
                due_date: this.state.date,
                projectId: this.state.destination
            })
            this.setState({
                taskText:'',
                descriptionText:'',
                date:'',
                addTask: false,
                destination: '2241494367'
            })

        }
        
    }

    
    render () {
        console.log(this.props)
        return (
            
            <Box  flex={10} className= 'inbox-back'>
                <Typography variant='h6' sx={{marginTop: '3%',textAlign:'center', position:'relative'}}>Inbox</Typography>
                    <>
                        {!this.props.current?null:
                        <>
                        {this.props.current.getStatus || this.props.current.postStatus ?
                         <LinearProgress /> : null
                        }
                        </>}
                    </>
                
                
                    <Box>
                    {!this.props.current ? null : 
                    <>
                    {this.props.current.inboxTasks.map((elem)=>{
                        if (this.state.editActive !== elem.id) {
                            return  <Box sx={{display:'flex',justifyContent:'center'}} key={elem.id} style={{width:'83%',paddingLeft:'30px'}}>
                        
                        <List >
                        <ListItem>
                            <Stack direction='row' justifyContent='space-between' alignItems='center' spacing={5} >
                                <Checkbox icon={<CircleOutlinedIcon/>} onChange={this.completeHandler} id={elem.id}></Checkbox>
                                <Typography variant='p' sx={{width:'30vw'}}>{elem.content}</Typography>
                                <Stack direction='row' spacing={2} >
                                    <ModeEditOutlinedIcon onClick={()=>{
                                        let date=''
                                        if (elem.due){
                                            date= elem.due.date
                                        }   
                                        this.editHandler({
                                            editId:elem.id,
                                            editName: elem.content,
                                            editDes: elem.description,
                                            editDate: date
                                            })
                                    }} sx={{cursor:'pointer'}}></ModeEditOutlinedIcon>
                                    <DeleteIcon  onClick={()=>{this.props.deleteTask(elem.id)}} sx={{cursor:'pointer'}}></DeleteIcon>
                                </Stack>
                            </Stack>
                        </ListItem>
                        <Divider></Divider>
                    </List>
                    </Box>
                        } else {
                            return  <>
                            <Container className="task-item" sx={{width:'65%', borderColor:'red'}} key={elem.id}>
                                <Stack padding={'20px'}>
                                    <InputBase placeholder="Task name" onChange={this.inputHandler} value={this.state.taskText}></InputBase>
                                    <InputBase placeholder="Description"  onChange={this.descriptionHandler} multiline value={this.state.descriptionText}></InputBase>
                                    <Box>
                                        <Stack>
                                        {this.state.dButtonVisible?
                                        <Stack>
                                            <Button color='primary' onClick={this.dateDisplayer} >Due Date</Button>
                                        </Stack>: null}
                                        {this.state.dateVisible ?
                                        <Box>
                                            <Stack>
                                                <InputBase type='Date'  className="date-picker" direction='row-reverse' onChange={this.dateHandler} value={this.state.date}></InputBase>
                                                <Button color='primary' onClick={this.cancelHandler}>Cancel</Button>
                                            </Stack>
                                            
                                        </Box>
                                        
                                         : null}
                                        </Stack> 
                                    </Box>  
                                </Stack>
                                
                            </Container>
                            <Stack direction='row' justifyContent='center' spacing={3} sx={{marginTop:'10px'}}>
                                <Button onClick={this.closeUpdateHandler} variant='contained'>Don't Update</Button>
                                <Button id = {elem.id} onClick={this.updateHandler} variant='contained'>Update</Button>
                            </Stack>
                            
                            </>
                        }
                        
                    })}
                    </>
                    }
                    
                    
                   
                </Box>
                <Stack>
                {this.state.addTask ? 
                <>
                <Container className="task-item" sx={{width:'65%', borderColor:'red'}}>
                    <Stack padding={'20px'}>
                        <InputBase placeholder="Task name" onChange={this.inputHandler}></InputBase>
                        <InputBase placeholder="Description"  onChange={this.descriptionHandler} multiline></InputBase>
                        <Box>
                            <Stack>
                            {this.state.dButtonVisible?
                            <Stack>
                                <Button color='primary' onClick={this.dateDisplayer}>Due Date</Button>
                            </Stack>: null}
                            {this.state.dateVisible ?
                            <Box>
                                <Stack>
                                    <InputBase type='Date'  className="date-picker" direction='row-reverse' onChange={this.dateHandler}></InputBase>
                                    <Button color='primary' onClick={this.cancelHandler}>Cancel</Button>
                                </Stack>
                                
                            </Box>
                            
                             : null}
                             <>
                                <InputLabel id="demo-simple-select-label">project</InputLabel>
                                <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value = {this.state.destination}
                                        
                                        placeholder='Project'
                                        onChange={this.destinationHandler}
                                        color={'primary'}
                                        sx={{color:"black"}}
        
                                        >
                                        
                                        {!this.props.current ? null : 
                                        this.props.current.projectsData.map((elem)=>{
                                            return <MenuItem value={elem.id} key={elem.id}>{elem.name} </MenuItem>
                                        })
                                        }
                                        
                                </Select>
                             </>
                             
                            </Stack> 
                        </Box>  
                    </Stack>
                    
                </Container>
                <Stack direction='row' justifyContent='center' spacing={3} sx={{marginTop:'10px'}}>
                    <Button onClick={this.closeHandler} variant='contained'>Cancel</Button>
                    <Button onClick={this.submitHandler} variant='contained'>Submit</Button>
                </Stack>
                
                </>
                
                
                
                : <Button onClick={this.addTaskHandler}><AddIcon></AddIcon>ADD TASK</Button>}
                </Stack>
                {/* {!this.props.current?null : 
                <> */}
                {/* {this.props.current.inboxTasks.length === 0 ? 
                <div className="d-flex justify-content-center ">
                    <img src='src/assets/Task-pana.png' className="w-50 h-75"></img>
                </div>
                :null}
                </>
                } */}
                
            </Box>
        )
    }
}

const mapStateToProps = (state)=>{
    return {current: state}
}

const mapDispatchToProps = (dispatch)=>{
    return {
        request: ()=>{dispatch()},
        recieved: ()=>{dispatch()},
        projectObtainer: ()=>{dispatch(getProjectHandler())},
        tasksObtainer: ()=>{dispatch(getTaskHandler())},
        addTask: (cont)=>{dispatch(postTaskHandler(cont))},
        completeTask: (id)=>{dispatch(completeTaskHandler(id))},
        deleteTask: (id)=>{dispatch(deleteTaskHandler(id))},
        updateTask: (id,content)=>{dispatch(updateTaskHandler(id,content))}
    }
}

const InboxCont = connect(mapStateToProps,mapDispatchToProps)(Inbox)

class WrapInbox extends React.Component {
    constructor(props) {
        super ()
    }
    render () {
        return (
            <Provider store={store}>
                <InboxCont></InboxCont>
            </Provider>
        )
    }
}

class InboxFinal extends React.Component {
    constructor(props){
        super()
    }
    render (){
        return (
            <>
        <SharedLayout></SharedLayout>
      <Stack direction='row'>
        <SideBar></SideBar>
        <WrapInbox></WrapInbox>
      </Stack>
        </>
        )
        
        
    }
}

export default InboxFinal