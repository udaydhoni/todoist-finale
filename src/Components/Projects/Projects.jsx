import React from 'react'
import SharedLayout from '../SharedLayout/SharedLayout'
import SideBar from '../SharedLayout/SideBar'
import { Box, Button, Divider, List, ListItem, Menu, MenuItem, Stack, TextField, Typography } from '@mui/material'
import { connect, Provider } from 'react-redux'
import store, { completeTaskHandler, deleteProjectHandler, deleteTaskHandler, getProjectHandler, getTaskHandler, postProjectHandler, postTaskHandler, updateProjectHandler, updateTaskHandler } from '../../Redux/Reducers'
import { Link } from 'react-router-dom'
import AddIcon from '@mui/icons-material/Add'
import Backdrop from '@mui/material/Backdrop'
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import MoreVertIcon from '@mui/icons-material/MoreVert';

import './Projects.css'

const style = {
    position: 'absolute',
    top: '40%',
    left: '65%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '1px solid #000',
    boxShadow: 24,
    p: 4,
    borderRadius: '20px'
  };

class Projects extends React.Component {
    constructor(props){
        super()
        this.state = {
            addProject: false,
            projectName: '',
            projectDescription: '',
            addDisabled: true,
            settings: '',
            editProject: ''
        }
    }
    componentDidMount () {
        this.props.projectObtainer()
    }
    nameHandler = (event)=>{
        let projectName = event.target.value
        if (projectName) {
            this.setState({
                projectName: projectName,
                addDisabled: false
            })
        } else {
            this.setState({
                addDisabled:true
            })
        }
    }
    descriptionHandler = (event)=>{
        this.setState({
            projectDescription: event.target.value
        })
    }
    addProjectHandler = ()=>{
        this.props.addProject(this.state.projectName)
        this.setState({
            addProject: false,
            projectName: '',
            projectDescription: '',
            addDisabled: true
        })
    }
    editProjectHandler = (obj)=>{
        this.setState({
            editProject:obj.editID,
            projectName: obj.editName
            
        })
        
        
    }
    deleteProjectHandler = (event)=>{
        console.log(event.target.id)
         this.props.deleteProject(`${event.target.id}`)
    }
    updateProjectHandler = (event)=>{
        this.props.updateProject(this.state.editProject,{name: this.state.projectName})
        this.setState({
            editProject:'',
            settings:''
        })
    }
    render () {
        console.log(this.props.current)
        return (
            <Box flex={10}>
                <h2 className='title'>Projects</h2>
                <div>
                    <div className='d-flex justify-content-end' style={{marginTop:'10%', paddingRight: '10%'}}>
                        <Button onClick={()=>{
                            this.setState({
                                addProject: true
                            })
                        }}><AddIcon></AddIcon>Add project</Button>
                    </div>
                    <Modal
                         aria-labelledby="transition-modal-title"
                         aria-describedby="transition-modal-description"
                         open={this.state.addProject}
                         onClose={()=>{
                            this.setState({
                                addProject:false
                            })
                         }}
                         closeAfterTransition
                         BackdropComponent={Backdrop}
                         BackdropProps={{
                           timeout: 500,
                         }}

                    >
                    <Fade in={this.state.addProject}>
                        <Box sx={style}>
                            <h6>Add Project</h6>
                            <Divider color={'secondary'}></Divider>
                            <TextField id="Name" label="Name"  sx={{width:'80%',borderRadius:'20px',mt:"4%"}} onChange={this.nameHandler}/>
                            <TextField id="Name" label="Description"  sx={{width:'80%',borderRadius:'20px',mt:'4%'}} onChange={this.descriptionHandler}/>
                            <div className='footer-buttons'>
                                <div>
                                    <Button color={'teritiary'} variant='contained' onClick={()=>{this.setState({addProject:false})}}>Cancel</Button>
                                    <Button color={'primary'} disabled={this.state.addDisabled} variant='contained' onClick={this.addProjectHandler}>Add</Button>
                                </div>
                            </div>
                        </Box>
                    </Fade>
                    </Modal>
                    {!this.props.current? null : 
                    <>
                        {this.props.current.projectsData.filter((project)=>{
                            return project.name !== 'Inbox'
                        }).map((otherProject)=>{
                            return (
                                <List key={otherProject.id}>
                                <div className='project-align'>
                                    <Link to={`/projects/${otherProject.id}`} className='project-name'>
                                    <ListItem>
                                    
                                        <div className = 'project-container'>
                                            <p>{otherProject.name}</p>
                                        </div>
                                        
                                    </ListItem>
                                    </Link>
                                    <Box sx={{position: 'relative'}}>
                                    <Button variant='contained' color='teritiary' id={otherProject.id} onClick={()=>{this.setState({settings:otherProject.id})}} sx={{position:'relative'}}><MoreVertIcon></MoreVertIcon></Button>
                                        <>
                                            {this.state.settings === otherProject.id ?
                                                <Menu
                                                id="basic-menu"
                                                // anchorEl={this.state.settings}
                                                open={Boolean(this.state.settings)}
                                                onClose={()=>{this.setState({settings:''})}}
                                                MenuListProps={{
                                                'aria-labelledby': 'basic-button',
                                                }}
                                                sx={{position:'absolute',top:'-530px',left:'1550px'}}
                                            >
                                                <MenuItem id={otherProject.id} onClick={()=>{this.editProjectHandler({
                                                    editID: otherProject.id,
                                                    editName: otherProject.name
                                                })}}>Edit Project</MenuItem>
                                                        <Modal
                                                            aria-labelledby="transition-modal-title"
                                                            aria-describedby="transition-modal-description"
                                                            open={Boolean(this.state.editProject)}
                                                            onClose={()=>{
                                                                this.setState({
                                                                    editProject:'',
                                                                    
                                                                })
                                                            }}
                                                            closeAfterTransition
                                                            BackdropComponent={Backdrop}
                                                            BackdropProps={{
                                                            timeout: 500,
                                                            }}

                                                        >
                                                        <Fade in={Boolean(this.state.editProject)}>
                                                            <Box sx={style}>
                                                                <h6>Edit Project</h6>
                                                                <Divider color={'secondary'}></Divider>
                                                                <TextField id="Name" label="Name"  sx={{width:'80%',borderRadius:'20px',mt:"4%"}} onChange={this.nameHandler} value={this.state.projectName}/>
                                                                <TextField id="Name" label="Description"  sx={{width:'80%',borderRadius:'20px',mt:'4%'}} onChange={this.descriptionHandler} value={this.state.projectDescription}/>
                                                                <div className='footer-buttons'>
                                                                    <div>
                                                                        <Button color={'teritiary'} variant='contained' onClick={()=>{this.setState({editProject:''})}}>Cancel</Button>
                                                                        <Button color={'primary'} disabled={this.state.addDisabled} variant='contained' onClick={this.updateProjectHandler}>Add</Button>
                                                                    </div>
                                                                </div>
                                                            </Box>
                                                        </Fade>
                                                        </Modal>
                                                <MenuItem id={otherProject.id} onClick={this.deleteProjectHandler}>Delete Project</MenuItem>
            
                                            </Menu>: null}
                                        </>
                                    </Box>
                                    
                                    
                                </div>
                            </List>
                            )
                        })}
                    </>
                    }
                </div>
                
            </Box>
            
        )
    }
}

const mapStateToProps = (state)=>{
    return {
        current: state
    }
}

const mapDispatchToProps = (dispatch)=>{
    return {
        projectObtainer: ()=>{dispatch(getProjectHandler())},
        tasksObtainer: ()=>{dispatch(getTaskHandler())},
        addTask: (cont)=>{dispatch(postTaskHandler(cont))},
        completeTask: (id)=>{dispatch(completeTaskHandler(id))},
        deleteTask: (id)=>{dispatch(deleteTaskHandler(id))},
        updateTask: (id,content)=>{dispatch(updateTaskHandler(id,content))},
        addProject: (cont)=>{dispatch(postProjectHandler(cont))},
        deleteProject: (id)=>{dispatch(deleteProjectHandler(id))},
        updateProject: (id,content)=>{dispatch(updateProjectHandler(id,content))}
    }
}

const ProjectsCont = connect(mapStateToProps,mapDispatchToProps)(Projects)

class ProjectConnect extends React.Component {
    constructor(props) {
        super()
    }
    render () {
        return (
            <Provider store={store}>
                <ProjectsCont></ProjectsCont>
            </Provider>
        )
    }
}

class WrapProjects extends React.Component {
    constructor(props){
        super()
    }
    render (){
        return (
            <>
             <SharedLayout></SharedLayout>
                <Stack direction='row'>
                    <SideBar></SideBar>
                    <ProjectConnect></ProjectConnect>
                </Stack>
        </>
        )
        
       
    }
}



export default WrapProjects