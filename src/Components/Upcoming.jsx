import React from "react";

import { Box, Button, InputBase, List, ListItem, ListItemButton, MenuItem, Select, Stack, TextField, Typography,Checkbox, Container } from "@mui/material";

import AddIcon from '@mui/icons-material/Add'
import TextareaAutosize from '@mui/base/TextareaAutosize';

import {  ThirtyFpsSelect } from "@mui/icons-material";
import CircleOutlinedIcon from '@mui/icons-material/CircleOutlined';
import DeleteIcon from '@mui/icons-material/Delete';
import ModeEditOutlinedIcon from '@mui/icons-material/ModeEditOutlined';
import { completeTaskHandler, deleteTaskHandler, postTaskHandler, updateTaskHandler} from "../Redux/Reducers";
// import { recieving_data } from "../Redux/Reducers";
import LinearProgress from '@mui/material/LinearProgress';
import {getProjectHandler,getTaskHandler } from "../Redux/Reducers";
import store from "../Redux/Reducers";
import { Provider, connect } from 'react-redux'
import SharedLayout from "./SharedLayout/SharedLayout";
import SideBar from "./SharedLayout/SideBar";
import Divider from '@mui/material/Divider'



class Upcoming extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            dateVisible: false,
            dButtonVisible: true,
            checked: false,
            addTask: '',
            taskText: '',
            descriptionText:'',
            editActive: false,
            date: '',
            destination: '2241494367'
        }
    }
   
    componentDidMount () {
        this.props.projectObtainer()
        this.props.tasksObtainer()
       
    }
    addTaskHandler = (event)=>{
        this.setState({
            addTask:event.target.id,
            date:event.target.id
        })
        
    }
    dateDisplayer = ()=>{
        this.setState({
            dateVisible: !this.state.dateVisible,
            dButtonVisible: this.state.dateVisible
        })
    }
    cancelHandler = ()=>{
        this.setState({
            dateVisible: !this.state.dateVisible,
            dButtonVisible: this.state.dateVisible
        })
    }
    closeHandler = ()=>{
        this.setState({
            addTask:false
        })
    }
    inputHandler = (event)=>{
        this.setState({
            taskText:event.target.value
        })
    }
    descriptionHandler = (event)=>{
        this.setState({
            descriptionText:event.target.value
        })
    }
    dateHandler = (event)=>{
        this.setState({
            date: event.target.value
        })
    }
    deleteHandler = (event)=>{
        console.log(event.target.id)
        
    }
    editHandler = (obj)=>{
        this.setState({
         editActive: obj.editId,
        taskText: obj.editName,
        descriptionText:obj.editDes,
        date:obj.editDate

        })
    }
    closeUpdateHandler = ()=>{
        this.setState({
            editActive: '',
        })
    }
    updateHandler = (event)=>{
        this.props.updateTask(event.target.id,{
            content: this.state.taskText,
            description: this.state.descriptionText,
            due_date: this.state.date
        })
        this.setState({
            taskText:'',
            descriptionText:'',
            date: '',
            editActive:''
        })
    }
    completeHandler = (event)=>{
        this.props.completeTask(event.target.id)
    }
    destinationHandler = (event)=>{
        this.setState({
            destination: event.target.value
        })
    }
    submitHandler = ()=>{
        if (this.state.taskText) {
            this.props.addTask({
                content:this.state.taskText,
                description: this.state.descriptionText,
                due_date: this.state.date,
                projectId: this.state.destination
            })
            this.setState({
                taskText:'',
                descriptionText:'',
                date:'',
                addTask: '',
                destination:'2241494367'
            })
            

        }
        
    }
    getTodaysDate = () => {
        const date = new Date()
        let day = date.getDate()
        if (Number(day) <= 9) {
            day = '0' + day
        }
        let month = date.getMonth() + 1
        if (Number(month) <= 9) {
            month = '0' + month
        }
        let year = date.getFullYear()
        let currentDate = `${year}-${month}-${day}`
        return currentDate
    }
    todaysDate = this.getTodaysDate()
    nextSevenDays = (date)=>{
        let array = [date]
        for (let day=1;day<7;day++) {
            array.push(date.slice(0,-2)+(Number(date.slice(-2))+day))
        }
        console.log(array)
        return array
    }
    next = this.nextSevenDays(this.todaysDate)
    

    
    render () {
        console.log(this.props)
        return (
            
            <Box  flex={10} >
                <Typography variant='h6' sx={{marginTop:'3%',textAlign:'center'}}>Upcoming</Typography>
                    <>
                        {!this.props.current?null:
                        <>
                        {this.props.current.getStatus || this.props.current.postStatus ?
                         <LinearProgress /> : null
                        }
                        </>}
                    </>
                
                
                    <Box>
                        {this.next.map((elem)=>{
                            console.log(elem)
                            return (
                                <>
                                    <Box key={elem}>
                                    <Typography variant='h6' sx={{marginTop:'3%',textAlign:'center'}}>{elem.slice(-2)+'-'+elem.slice(5,7)+'-'+elem.slice(0,4)}</Typography>
                                    <>
                                    {!this.props.current ? null : <>
                                    {this.props.current.tasksData.filter((task)=> {
                                        if (task.due) {
                                            return task.due.date === elem
                                        }
                                    }
                                    
                                    ).map((elem)=>{
                                        if (this.state.editActive !== elem.id) {
                                            return  <Box sx={{display:'flex',justifyContent:'center'}} key={elem.id}>
                                        
                                        <List >
                                        <ListItem>
                                            <Stack direction='row' justifyContent='space-between' alignItems='center' spacing={7}>
                                                <Checkbox icon={<CircleOutlinedIcon/>} onChange={this.completeHandler} id={elem.id}></Checkbox>
                                                <Typography variant='p' sx={{width:'30vw'}}>{elem.content}</Typography>
                                                <Stack direction='row' spacing={2}>
                                                    <ModeEditOutlinedIcon onClick={()=>{this.editHandler({
                                                        editId:elem.id,
                                                        editName: elem.content,
                                                        editDes: elem.description,
                                                        editDate: elem.due.date
                                                        })}} sx={{cursor:'pointer'}}></ModeEditOutlinedIcon>
                                                    <DeleteIcon  onClick={()=>{this.props.deleteTask(elem.id)}} sx={{cursor:'pointer'}}></DeleteIcon>
                                                </Stack>
                                            </Stack>
                                        </ListItem>
                                        <Divider></Divider>
                                    </List>
                                    </Box>
                                        } else {
                                            return  <>
                                            <Container className="task-item" sx={{width:'65%', borderColor:'red'}} key={elem.id}>
                                                <Stack padding={'20px'}>
                                                    <InputBase placeholder="Task name" onChange={this.inputHandler} value={this.state.taskText}></InputBase>
                                                    <InputBase placeholder="Description"  onChange={this.descriptionHandler} multiline value={this.state.descriptionText}></InputBase>
                                                    <Box>
                                                        <Stack>
                                                        {this.state.dButtonVisible?
                                                        <Stack>
                                                            <Button color='primary' onClick={this.dateDisplayer}>Due Date</Button>
                                                        </Stack>: null}
                                                        {this.state.dateVisible ?
                                                        <Box>
                                                            <Stack>
                                                                <InputBase type='Date'  className="date-picker" direction='row-reverse' onChange={this.dateHandler} value={elem.due.date}></InputBase>
                                                                <Button color='primary' onClick={this.cancelHandler}>Cancel</Button>
                                                            </Stack>
                                                            
                                                        </Box>
                                                        
                                                         : null}
                                                        </Stack> 
                                                    </Box>  
                                                </Stack>
                                                
                                            </Container>
                                            <Stack direction='row' justifyContent='center' spacing={3} sx={{marginTop:'10px'}}>
                                                <Button onClick={this.closeUpdateHandler} variant='contained'>Don't Update</Button>
                                                <Button id = {elem.id} onClick={this.updateHandler} variant='contained'>Update</Button>
                                            </Stack>
                                            
                                            </>
                                        }
                                        
                                    })}
                                    </>}
                                    </>
                                </Box>
                                <Stack>
                                {this.state.addTask === elem ? 
                                <>
                                <Container className="task-item" sx={{width:'65%', borderColor:'red'}}>
                                    <Stack padding={'20px'}>
                                        <InputBase placeholder="Task name" onChange={this.inputHandler}></InputBase>
                                        <InputBase placeholder="Description"  onChange={this.descriptionHandler} multiline></InputBase>
                                        <Box>
                                            <Stack>
                                            {this.state.dButtonVisible?
                                            <Stack>
                                                <Button color='primary' onClick={this.dateDisplayer}>Due Date</Button>
                                            </Stack>: null}
                                            {this.state.dateVisible ?
                                            <Box>
                                                <Stack>
                                                    <InputBase type='Date'  className="date-picker" direction='row-reverse' onChange={this.dateHandler} value={this.state.date}></InputBase>
                                                    <Button color='primary' onClick={this.cancelHandler}>Cancel</Button>
                                                </Stack>
                                                
                                            </Box>
                                            
                                             : null}
                                             <>
                                {/* <InputLabel id="demo-simple-select-label">Age</InputLabel> */}
                                <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value = {this.state.destination}
                                        label='project'
                                        placeholder='Project'
                                        onChange={this.destinationHandler}
                                        color={'primary'}
                                        >
                                        
                                        {!this.props.current ? null : 
                                        this.props.current.projectsData.map((elem)=>{
                                            return <MenuItem value={elem.id} key={elem.id}>{elem.name} </MenuItem>
                                        })
                                        }
                                        
                                </Select>
                             </>
                                            </Stack> 
                                        </Box>  
                                    </Stack>
                                    
                                </Container>
                                <Stack direction='row' justifyContent='center' spacing={3} sx={{marginTop:'10px'}}>
                                    <Button onClick={this.closeHandler} variant='contained'>Cancel</Button>
                                    <Button onClick={this.submitHandler} variant='contained'>Submit</Button>
                                </Stack>
                                
                                </>
                                
                                
                                
                                : <Button onClick={this.addTaskHandler} id={elem}><AddIcon></AddIcon>ADD TASK</Button>}
                                </Stack>
                                </>
                                
                                
                            )
                        })}
                    
                    
                    
                   
                </Box>
                
                
                
            </Box>
        )
    }
}

const mapStateToProps = (state)=>{
    return {current: state}
}

const mapDispatchToProps = (dispatch)=>{
    return {
        request: ()=>{dispatch()},
        recieved: ()=>{dispatch()},
        projectObtainer: ()=>{dispatch(getProjectHandler())},
        tasksObtainer: ()=>{dispatch(getTaskHandler())},
        addTask: (cont)=>{dispatch(postTaskHandler(cont))},
        completeTask: (id)=>{dispatch(completeTaskHandler(id))},
        deleteTask: (id)=>{dispatch(deleteTaskHandler(id))},
        updateTask: (id,content)=>{dispatch(updateTaskHandler(id,content))}
    }
}

const UpcomingCont = connect(mapStateToProps,mapDispatchToProps)(Upcoming)

class WrapUpcoming extends React.Component {
    constructor(props) {
        super ()
    }
    render () {
        return (
            <Provider store={store}>
                <UpcomingCont></UpcomingCont>
            </Provider>
        )
    }
}

class UpcomingFinal extends React.Component {
    constructor(props){
        super()
    }
    render (){
        return (
            <>
        <SharedLayout></SharedLayout>
      <Stack direction='row'>
        <SideBar></SideBar>
        <WrapUpcoming></WrapUpcoming>
      </Stack>
        </>
        )
        
        
    }
}

export default UpcomingFinal