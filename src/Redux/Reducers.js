import React from 'react'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { TodoistApi } from '@doist/todoist-api-typescript'
const api = new TodoistApi('b14c0d663e8d1fa3b3aa21ac9477953835188e86')

const INITIAL_STATE = {
    projectsData:[],
    tasksData:[],
    inboxTasks: [],
    getStatus: '',
    postStatus: ''
}
 const requesting_data = (kind)=>{
    return {
        type: 'REQUESTING',
        kind:kind
    }
}

 const recieving_data = (info,kind)=>{
    return {
        type: 'RECIEVING',
        value: info,
        kind: kind
    }
}

 const post_request = (kind)=>{
    return {
        type:'POSTING',
        kind:kind
    }
}

 const post_response = (data,kind)=>{
    return {
        type: 'RESPONSEDATA',
        value:data,
        kind: kind
    }
}

const complete_task = (id)=>{
    return {
        type: 'COMPLETE',
        id: id
    }
}

const delete_task = (id)=>{
    return {
        type: 'DELETE',
        id: id,
        
    }
}
const delete_project = (id)=>{
    return {
        type: 'DELETEPROJECT',
        id:id
    }
}
const update_task = (id,info)=>{
    return {
        type: 'UPDATE',
        id:id,
        value: info
    }
}

const update_project = (id,info)=>{
    return {
        type: 'UPDATEPROJECT',
        id:id,
        value: info
    }
}

export const getProjectHandler = ()=>{
    return function (dispatch) {
        dispatch (requesting_data('projects'))
        api.getProjects()
            .then((projects)=> dispatch(recieving_data(projects,'projects')))
            .catch((err)=>console.log(err))
    }
}

export const getTaskHandler = ()=>{
    return function (dispatch) {
        dispatch (requesting_data('tasks'))
        api.getTasks()
            .then((tasks)=>dispatch(recieving_data(tasks,'tasks')))
            .catch ((err)=>console.log(err))
    }
}

export const postTaskHandler = (cont)=>{
    return function (dispatch) {
        dispatch(post_request('tasks'))
        api.addTask(cont)
            .then((task)=>dispatch(post_response(task,'tasks')))
            .catch((err)=>console.log(err))
    }
}

export const postProjectHandler = (cont)=>{
    return function (dispatch) {
        dispatch(post_request('projects'))
        api.addProject({name: cont})
            .then((project)=>dispatch(post_response(project,'projects')))
            .catch((err)=>console.log(err))
    }
}

export const completeTaskHandler = (idTask)=>{
    return function(dispatch) {
        dispatch(post_request('tasks'))
        api.closeTask(idTask)
            .then((suc)=> dispatch(complete_task(idTask)))
            .catch((err)=>console.log(err))
    }
}

export const deleteTaskHandler = (idTask)=>{
    return function(dispatch) {
        dispatch(post_request('tasks'))
        api.deleteTask(idTask)
            .then((suc)=> dispatch(delete_task(idTask)))
            .catch((err)=>console.log(err))
    }
}

export const updateTaskHandler = (id,updateObject)=>{
    return function(dispatch) {
        dispatch(post_request('tasks'))
        api.updateTask(id,updateObject)
            .then((suc)=> {return api.getTask(id)})
            .then((res)=>dispatch(update_task(id,res)))
            .catch((err)=>console.log(err))
    }
}
export const deleteProjectHandler = (id)=>{
    return function(dispatch) {
        dispatch(post_request('tasks'))
    api.deleteProject(id)
        .then((res)=>dispatch(delete_project(id)))
        .catch((err)=>console.log(err))
    }
    
}
export const updateProjectHandler = (id,updateObject)=>{
    return function(dispatch) {
        dispatch(post_request('tasks'))
        api.updateProject(id,updateObject)
            .then((suc)=> {return api.getProject(id)})
            .then((res)=>dispatch(update_project(id,res)))
            .catch((err)=>console.log(err))
    }
}

export const rootReducer = (state=INITIAL_STATE,action)=>{
    let stringObject = JSON.stringify(state)
    let newObject = JSON.parse(stringObject)
    
    switch(action.type) {
        case 'REQUESTING':
            console.log('itsrequesting')
            if (action.kind === 'projects') {
                newObject.projectsData = []
            } else {
                newObject.tasksData = []
                newObject['getStatus'] =true
            }
            newObject.inboxTasks = []
            return newObject
        case 'POSTING':
                console.log('yo boy')
                newObject['postStatus'] = true
                return newObject
        case 'RECIEVING':
            newObject.getStatus= ''
            if (action.kind === 'projects') {
                newObject.projectsData = [...newObject.projectsData,...action.value]
            } else {
                newObject.tasksData = [...newObject.tasksData,...action.value]
            }
            if (newObject.projectsData.length && newObject.tasksData.length) {
                let inboxId = newObject.projectsData.find((elem)=>elem.name === 'Inbox').id
                let filteredData = newObject.tasksData.filter((elem)=> elem.projectId===inboxId)
                newObject['inboxTasks'] = filteredData
            }
            newObject['getStatus'] =false
            newObject['postStatus'] = false
            return newObject
        case 'RESPONSEDATA':
            if (action.kind == 'tasks') {
                newObject.tasksData= [...newObject.tasksData,action.value]
                newObject['getStatus'] =false
                newObject['postStatus'] = false
                return newObject
            }
            else {
                console.log('happening')
                newObject.projectsData= [...newObject.projectsData,action.value]
                newObject['getStatus'] =false
                newObject['postStatus'] = false
                return newObject 
            }
        case 'UPDATE':
            console.log('happpening')
            for (let index = 0; index<newObject.tasksData.length;index++) {
                if (newObject.tasksData[index]['id'] == action.id) {
                    newObject.tasksData[index] = action.value
                    break
                }
            }
            newObject['getStatus'] =false
            newObject['postStatus'] = false
            
            return newObject
        case 'COMPLETE':
            newObject.tasksData = newObject.tasksData.filter((elem)=> elem.id!== action.id)
            newObject.inboxTasks = newObject.inboxTasks.filter((elem)=> elem.id!== action.id)
            newObject.getStatus =false
            newObject['postStatus'] = false
            return newObject
        case 'DELETE':
            newObject.tasksData = newObject.tasksData.filter((elem)=> elem.id!== action.id)
            newObject.inboxTasks = newObject.inboxTasks.filter((elem)=> elem.id!== action.id)
            newObject.getStatus =false
            newObject['postStatus'] = false
            return newObject
        case 'DELETEPROJECT':

            newObject.projectsData = newObject.projectsData.filter((elem)=> elem.id!== action.id)
            console.log('happende',newObject)
            newObject['getStatus'] =false
            newObject['postStatus'] = false
            return newObject
        case 'UPDATEPROJECT':
            for (let index = 0; index<newObject.projectsData.length;index++) {
                if (newObject.projectsData[index]['id'] == action.id) {
                    newObject.projectsData[index] = action.value
                    break
                }
            }
            newObject['getStatus'] =false
            newObject['postStatus'] = false
            return newObject
        
        default:
            return state
    }
}
const store = createStore(rootReducer,applyMiddleware(thunk))

export default store